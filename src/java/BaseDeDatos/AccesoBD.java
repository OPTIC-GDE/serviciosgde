/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BaseDeDatos;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;

/**
 *
 * @author joansme
 */
public class AccesoBD {

    private static String CREACION = "create table ambiente(idAmbiente int(11) auto_increment, nombre varchar(10),CONSTRAINT ambiente_pk PRIMARY key (idAmbiente));\n"
            + "\n"
            + "create table direccion(idDireccion int(11) auto_increment, idAmbiente int(11), servicio varchar(60), url varchar(256),\n"
            + "CONSTRAINT ambiente_fk foreign key (idAmbiente) references ambiente(idAmbiente), \n"
            + "CONSTRAINT direccion_pk  primary key(idDireccion));";
    private static String URL = "192.168.24.171";
    private static String PORT = ":3306";
        private static String USER = "wsgdeprod";
        private static String PASS = "Bb7Ok5j5rd";
    private static String JDBC = "jdbc:mariadb://";
    private static String BD = "/wsgdeprod";

    private static Connection conectar() throws SQLException, ClassNotFoundException {
        Connection conexion = null;
        Class.forName("org.mariadb.jdbc.Driver");
        Credenciales credenciales = new CredencialesProduccion();        
        //Credenciales credenciales = new CredencialesTesting();

        conexion = DriverManager.getConnection(JDBC + credenciales.URL + credenciales.PORT + credenciales.BD, 
                credenciales.USER, credenciales.PASS);
        
//conexion = DriverManager.getConnection(JDBC + URL + PORT + BD, USER, PASS);
        return conexion;
    }

    private static void desconectar(Connection conexion) throws SQLException {
        conexion.close();
    }


    //Servicio -> url
    public static HashMap<String, String> obtenerDirecciones(String ambiente) throws SQLException, ClassNotFoundException, Exception {
        Statement st;
        Connection con = conectar();
        st = con.createStatement();
        java.sql.ResultSet resultSet;
        String consulta;
        if( !ambiente.equalsIgnoreCase("prodv4") && !ambiente.equalsIgnoreCase("test") && !ambiente.equalsIgnoreCase("cap") && !ambiente.equalsIgnoreCase("prod") ){
            throw new Exception("Los ambientes habilitados son test,cap y prod");
        }
        consulta = " SELECT servicio,url"
                + "  FROM ambiente a inner join direccion d on d.idAmbiente = a.idAmbiente "
                + "  WHERE a.nombre = '" + ambiente + " ' ";
        resultSet = st.executeQuery(consulta);
        HashMap<String,String> res = recorrerResultSet(resultSet);
        desconectar(con);
        return res;
    }

    private static HashMap<String, String> recorrerResultSet(java.sql.ResultSet resultSet) throws SQLException {
        HashMap<String, String> mapa = new HashMap<>();
        while (resultSet != null && resultSet.next()) {
            mapa.put(resultSet.getString("servicio"),resultSet.getString("url"));
        }
        return mapa;

    }

}
