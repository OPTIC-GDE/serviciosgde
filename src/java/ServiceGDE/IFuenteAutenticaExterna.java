/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ServiceGDE;

import BaseDeDatos.AccesoBD;
import Parametros.Mensaje;
import Parametros.Servicios;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import java.util.HashMap;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import utiles.RSAUtil;
import utiles.RespuestaUser;
import utiles.TResultadoServicioFA;
import utiles.UserValidator;

/**
 *
 * @author joans
 */
@WebService(serviceName = "IFuenteAutenticaExterna")
public class IFuenteAutenticaExterna {

    public IFuenteAutenticaExterna() {
    }

    /**
     *
     * @param Servicio ver los string en la clase Parametros.servicio.java
     * @param DatoAuditoria
     * @param Cuerpo
     * @return
     */
    @WebMethod(operationName = "Solicitar_Servicio_FA")
    public TResultadoServicioFA Solicitar_Servicio_FA(@WebParam(name = "Servicio") String Servicio, @WebParam(name = "DatoAuditoria") String DatoAuditoria, @WebParam(name = "Cuerpo") byte[] Cuerpo) {
        String res = "";
        TResultadoServicioFA respuesta = new TResultadoServicioFA();
        try {
            JsonObject parametrosJson;
            Gson gson = new Gson();
            if (Cuerpo == null) {
                throw new Exception("Por favor envie algo en el cuerpo");
            }
            if (Servicio == null) {
                throw new Exception("Por favor envie algun tipo de servicio puede ser vincularDocumento, generarExpediente, generarTarea, generarDocumento, generarPase, consultarGedo, consultarExpediente");
            }
            /**
             * Obtengo el cuerpo del mensaje que viene en formato json
             */
            String cuerpoString = new String(Cuerpo);
            Mensaje msg = gson.fromJson(cuerpoString, Mensaje.class);
            String parametros = msg.getParametros();
            parametrosJson = gson.fromJson(msg.getParametros(), JsonObject.class);
            /**
             * Se valida el usuario, si envia ticket de onlogin usa onlogin sino
             * usar usuario y contrasenia
             */

            RespuestaUser validacionUsuario;
            if (msg.getTicket() != null) {
                validacionUsuario = UserValidator.validar(msg.getTicket());
            } else {
                //System.out.println("Validacion por usuario y contrasenia");
                msg.setClave(obtenerClave(msg));

                String usuario = parametrosJson.getAsJsonPrimitive("usuario").getAsString();
                //System.out.println("usuario "+ usuario);

                String ambiente = parametrosJson.getAsJsonPrimitive("ambiente").getAsString();
                //System.out.println("ambiente " + ambiente);

                HashMap<String, String> direcciones = AccesoBD.obtenerDirecciones(ambiente);
                //  System.out.println("Ldap " + direcciones.get(Servicios.ldap));                
                //  System.out.println("clave " + msg.getClave());
                validacionUsuario = UserValidator.validar(usuario, msg.getClave(), direcciones.get(Servicios.ldap));
                //  System.out.println("Esta conectado "+validacionUsuario.isConectado());

            }
            if (validacionUsuario.isConectado()) {
                parametrosJson.addProperty("usuario", validacionUsuario.getUsuario());
                parametros = gson.toJson(parametrosJson);
                switch (Servicio) {
                    case (Servicios.vincularDocumento):
                        res = ServiciosController.vincularDocumento(parametros, msg.getClave());
                        respuesta.setCodResultado(0);
                        break;
                    case (Servicios.generarExpediente):
                        res = ServiciosController.generarExpediente(parametros, msg.getClave());
                        respuesta.setCodResultado(0);
                        break;
                    case (Servicios.generarTarea):
                        res = ServiciosController.generarTarea(parametros, msg.getArchivo(), msg.getClave());
                        respuesta.setCodResultado(0);
                        break;
                    case (Servicios.generarDocumento):
                        res = ServiciosController.generarDocumento(parametros, msg.getArchivo(), msg.getClave());
                        respuesta.setCodResultado(0);
                        break;
                    case (Servicios.generarPase):
                        res = ServiciosController.generarPase(parametros, msg.getClave());
                        respuesta.setCodResultado(0);
                        break;
                    case (Servicios.consultaGedo):
                        res = ServiciosController.consultarGedo(parametros, msg.getClave());
                        respuesta.setCodResultado(0);
                        break;
                    case (Servicios.descargarGedo):
                        res = ServiciosController.descargarDocumento(parametros, msg.getClave());
                        respuesta.setCodResultado(0);
                        break;
                    case (Servicios.asociarExpedientes):
                        res = ServiciosController.asociarExpedientes(parametros, msg.getClave());
                        respuesta.setCodResultado(0);
                        break;
                    case (Servicios.consultarExpedientes):
                        res = ServiciosController.consultarExpediente(parametros, msg.getClave());
                        respuesta.setCodResultado(0);
                        break;
                    case (Servicios.bloqueoDeExpedientes):
                        respuesta.setCodResultado(1);
                        res = "Este servicio no esta implementado aun";
                        break;
                    case (Servicios.detalleExpediente):
                        res = ServiciosController.detalleExpediente(parametros, msg.getClave());
                        respuesta.setCodResultado(0);
                        break;
                    default:
                        res = "El nombre del servicio: " + Servicio + " no es correcto los servicios pueden ser asociarExpedientes, vincularDocumento, generarExpediente, generarTarea, generarDocumento, generarPase, consultarGedo,consultarExpediente";
                        respuesta.setCodResultado(1);
                }
            } else {
                respuesta.setCodResultado(1);
                respuesta.setMensajeResultado("El usuario gde o el ticket de onelogin es incorrecto");
                res = "El usuario o el ticket es incorrecto";
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            res = ex.getMessage();
            respuesta.setCodResultado(1);
            respuesta.setMensajeResultado(res);

            if (res == null && Cuerpo != null) {
                res += "Hubo un error con los parametros "
                        + "DatosAuditoria: " + DatoAuditoria + " Cuerpo: " + new String(Cuerpo) + " Servicio: " + Servicio;
            } else {
                res += "\n Hubo un error interno en el servidor con los parametros "
                        + "DatosAuditoria: " + DatoAuditoria + " Cuerpo: " + new String(Cuerpo) + " Servicio: " + Servicio;
            }

        } finally {
            respuesta.setResultado1(res.getBytes());
            return respuesta;
        }

    }

    private String obtenerClave(Mensaje mensaje) throws Exception {
        String clave = RSAUtil.decrypt(mensaje.getClave());
        return clave;
    }

}
