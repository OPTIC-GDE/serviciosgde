/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ServiceGDE;

import BaseDeDatos.AccesoBD;
import Parametros.Bloqueador;
import Parametros.GenerarDocumento;
import Parametros.GenerarExpediente;
import Parametros.GenerarPase;
import Parametros.AsociarExpediente;
import Parametros.GenerarTarea;
import Parametros.ConsultaDocumento;
import Parametros.Servicios;
import Parametros.DetalleExpediente;
import Parametros.VincularDocumento;
import Parametros.DescargarDocumento;
import Parametros.ConsultaExpedientes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;
import java.util.HashMap;
import utiles.AnnotatedDeserializer;
import utiles.UserValidator;
import utiles.utiles;

/**
 *
 * @author joans
 */
public class ServiciosController {

    public static String vincularDocumento(String parametros, String clave) throws Exception {
        Gson gson;
        gson = new GsonBuilder()
                .registerTypeAdapter(VincularDocumento.class, new AnnotatedDeserializer<VincularDocumento>())
                .create();
        VincularDocumento pars = gson.fromJson(parametros, VincularDocumento.class);
        if (pars.getAmbiente().equalsIgnoreCase("prod")) {

            System.out.println("se ejecuto el servicio Vincular Documento: " + "  " + (new Date()).toString() + ": " + gson.toJson(pars));
        }
        HashMap<String, String> direcciones = AccesoBD.obtenerDirecciones(pars.getAmbiente());
        String result = documentosoficialesws.DocumentosOficialesWS.main(pars.toParametros(direcciones));
        return result;
    }

    public static String generarPase(String parametros, String clave) throws Exception {
        Gson gson;
        gson = new GsonBuilder()
                .registerTypeAdapter(GenerarPase.class, new AnnotatedDeserializer<GenerarPase>())
                .create();
        GenerarPase pars = gson.fromJson(parametros, GenerarPase.class);
        String result;
        HashMap<String, String> direcciones = AccesoBD.obtenerDirecciones(pars.getAmbiente());
        if (pars.getAmbiente().equalsIgnoreCase("prod")) {

            System.out.println("se ejecuto el servicio Generar Pase: " + "  " + (new Date()).toString() + ": " + gson.toJson(pars));
        }
        return generarpase.GenerarPase.main2(pars.toParametros(direcciones));
    }

    public static String descargarDocumento(String parametros, String clave) throws Exception {
        Gson gson;
        gson = new GsonBuilder()
                .registerTypeAdapter(DescargarDocumento.class, new AnnotatedDeserializer<GenerarPase>())
                .create();
        DescargarDocumento pars = gson.fromJson(parametros, DescargarDocumento.class);
        String result;
        HashMap<String, String> direcciones = AccesoBD.obtenerDirecciones(pars.getAmbiente());
        String[] args = pars.toParametros(direcciones);
        if (pars.getAmbiente().equalsIgnoreCase("prod") || pars.getAmbiente().equalsIgnoreCase("test")) {
            System.out.println("se ejecuto el servicio Descargar documento: " + "  " + (new Date()).toString() + ": " + gson.toJson(pars));
            String arg = "";
            for (int i = 0; i < args.length; i++) {
                arg += args[i];
            }
            System.out.println((new Date()).toString() + ": " + arg);

        }
        return descargardocumentows.DescargarDocumentoWS.libreriaMain(args);
    }

    public static String asociarExpedientes(String parametros, String clave) throws Exception {
        Gson gson, gson2;
        gson = new GsonBuilder()
                .registerTypeAdapter(AsociarExpediente.class, new AnnotatedDeserializer<AsociarExpediente>())
                .create();
        gson2 = new Gson();
        // JsonObject expediente = new JsonObject();
        AsociarExpediente pars;
        pars = gson.fromJson(parametros, AsociarExpediente.class);
        String result;
        if (pars.getAmbiente().equalsIgnoreCase("prod")) {
            System.out.println("se ejecuto el servicio Asociar Expediente: " + "  " + (new Date()).toString() + ": " + gson.toJson(pars));
        }
        HashMap<String, String> direcciones = AccesoBD.obtenerDirecciones(pars.getAmbiente());
        return asociarexpedientews.AsociarExpedienteWS.main2(pars.toParametros(direcciones));
    }

    public static String generarExpediente(String parametros, String clave) throws Exception {
        Gson gson;
        gson = new GsonBuilder()
                .registerTypeAdapter(GenerarExpediente.class, new AnnotatedDeserializer<GenerarExpediente>())
                .create();
        GenerarExpediente pars;
        pars = gson.fromJson(parametros, GenerarExpediente.class);
        String result;
        if (pars.getAmbiente().equalsIgnoreCase("prod")) {
            System.out.println("se ejecuto el servicio Generar Expediente: " + "  " + (new Date()).toString() + ": " + gson.toJson(pars));
        }
        HashMap<String, String> direcciones = AccesoBD.obtenerDirecciones(pars.getAmbiente());
        return generarexpediente.GenerarCaratula.main2(pars.toParametros(direcciones));
    }

    public static String consultarExpediente(String parametros, String clave) throws Exception {
        Gson gson;
        gson = new GsonBuilder()
                .registerTypeAdapter(ConsultaExpedientes.class, new AnnotatedDeserializer<GenerarExpediente>())
                .create();
        ConsultaExpedientes pars;
        pars = gson.fromJson(parametros, ConsultaExpedientes.class);
        String result;
        if (pars.getAmbiente().equalsIgnoreCase("prod") || pars.getAmbiente().equalsIgnoreCase("test")) {
            System.out.println("se ejecuto el servicio Consultar Expedientes: " + "  " + (new Date()).toString() + ": " + gson.toJson(pars));
        }
        HashMap<String, String> direcciones = AccesoBD.obtenerDirecciones(pars.getAmbiente());
//        boolean user = UserValidator.validarUsuario(pars.getUsuario(), clave, direcciones.get(Servicios.ldap));
        //      if (user) {
        String ipTest = "http://192.168.24.134";
        String ipProd = "http://192.168.12.234";
        String direccion = ipProd + "/consultas/expedientes" + pars.toParametros();
        URL urlConsulta = new URL(direccion);
        HttpURLConnection con = (HttpURLConnection) urlConsulta.openConnection();
        con.setRequestProperty("content-type", "application/json");
        con.setRequestMethod("GET");

        InputStream responseStream = utiles.dispatch(con);
        InputStreamReader isReader = new InputStreamReader(responseStream);

        BufferedReader reader = new BufferedReader(isReader);
        StringBuilder sb = new StringBuilder();
        String str;
        while ((str = reader.readLine()) != null) {
            sb.append(str);
        }
        result = sb.toString();

        return result;
    }

    public static String detalleExpediente(String parametros, String clave) throws Exception {
        Gson gson;
        gson = new GsonBuilder()
                .registerTypeAdapter(DetalleExpediente.class, new AnnotatedDeserializer<GenerarExpediente>())
                .create();
        DetalleExpediente pars;
        pars = gson.fromJson(parametros, DetalleExpediente.class);
        String result;
        if (pars.getAmbiente().equalsIgnoreCase("prod") || pars.getAmbiente().equalsIgnoreCase("test")) {
            System.out.println("se ejecuto el servicio Consultar Expedientes: " + "  " + (new Date()).toString() + ": " + gson.toJson(pars));
        }
        HashMap<String, String> direcciones = AccesoBD.obtenerDirecciones(pars.getAmbiente());

        String ipTest = "http://192.168.24.134";
        String ipProd = "http://192.168.12.234";
        String direccion = ipProd + "/consultas/expediente" + pars.toParametros();
        URL urlConsulta = new URL(direccion);
        HttpURLConnection con = (HttpURLConnection) urlConsulta.openConnection();
        con.setRequestProperty("content-type", "application/json");
        con.setRequestMethod("GET");

        InputStream responseStream = utiles.dispatch(con);

        InputStreamReader isReader = new InputStreamReader(responseStream);

        BufferedReader reader = new BufferedReader(isReader);
        StringBuilder sb = new StringBuilder();
        String str;
        while ((str = reader.readLine()) != null) {
            sb.append(str);
        }
        result = sb.toString();

        return result;
    }

    public static String generarTarea(String parametros, byte[] archivo, String clave) throws Exception {
        Gson gson;
        gson = new GsonBuilder()
                .registerTypeAdapter(GenerarTarea.class, new AnnotatedDeserializer<GenerarTarea>())
                .create();

        GenerarTarea pars;
        pars = gson.fromJson(parametros, GenerarTarea.class);
        // JsonObject generarTarea = new JsonObject();
        String result;
        HashMap<String, String> direcciones = AccesoBD.obtenerDirecciones(pars.getAmbiente());
        if (pars.getAmbiente().equalsIgnoreCase("prod")) {
            System.out.println("se ejecuto el servicio Generar Tarea: " + "  " + (new Date()).toString() + ": " + gson.toJson(pars));
        }
        // boolean user = UserValidator.validarUsuario(pars.getUsuario(), clave, direcciones.get(Servicios.ldap));
        // if (user) {
        // generarTarea.addProperty("nroProceso");
        result = generartarea.GenerarTarea.generarTarea(pars.toParametros(direcciones), archivo);

        /*} else {
            result = "El usuario gde no existe o la contrasenia es incorrecta";
            throw new Exception(result);
        }*/
        return result;
    }

    public static String generarDocumento(String parametros, byte[] archivo, String clave) throws Exception {
        Gson gson;
        gson = new GsonBuilder()
                .registerTypeAdapter(GenerarDocumento.class, new AnnotatedDeserializer<GenerarDocumento>())
                .create();
        GenerarDocumento pars;
        pars = gson.fromJson(parametros, GenerarDocumento.class);
        String result;
        if (pars.getAmbiente().equalsIgnoreCase("prod")) {
            System.out.println("se ejecuto el servicio Generar Documento: " + "  " + (new Date()).toString() + ": " + gson.toJson(pars));
        }
        HashMap<String, String> direcciones = AccesoBD.obtenerDirecciones(pars.getAmbiente());
        /*boolean user = UserValidator.validarUsuario(pars.getUsuario(), clave, direcciones.get(Servicios.ldap));
        if (user) {
            result = generardocumentows.GenerarDocumentoWS.main2(pars.toParametros(direcciones), archivo);
        } else {
            result = "El usuario gde no existe o la contrasenia es incorrecta";
            throw new Exception(result);
        }*/
        return generardocumentows.GenerarDocumentoWS.main2(pars.toParametros(direcciones), archivo);
    }

    public static String bloquear(String parametros, String clave) throws Exception {
        Gson gson;
        gson = new GsonBuilder()
                .registerTypeAdapter(Bloqueador.class, new AnnotatedDeserializer<Bloqueador>())
                .create();
        Bloqueador pars;
        pars = gson.fromJson(parametros, Bloqueador.class);
        String result;
        if (pars.getAmbiente().equalsIgnoreCase("prod")) {
            System.out.println((new Date()).toString() + ": " + gson.toJson(pars));
        }
        HashMap<String, String> direcciones = AccesoBD.obtenerDirecciones(pars.getAmbiente());
     /*   boolean user = UserValidator.validarUsuario(pars.getUsuario(), clave, direcciones.get(Servicios.ldap));
        if (user) {
            result = main.main.main2(pars.toParametros(direcciones));
        } else {
            result = "El usuario gde no existe o la contrasenia es incorrecta";
            throw new Exception(result);
        }*/
        return main.main.main2(pars.toParametros(direcciones));
    }

    public static String consultarGedo(String parametros, String clave) throws Exception {
        Gson gson;
        gson = new GsonBuilder()
                .registerTypeAdapter(ConsultaDocumento.class, new AnnotatedDeserializer<ConsultaDocumento>())
                .create();
        ConsultaDocumento pars;
        pars = gson.fromJson(parametros, ConsultaDocumento.class);
        String result;
        HashMap<String, String> direcciones = AccesoBD.obtenerDirecciones(pars.getAmbiente());
        /*if (pars.getAmbiente().equalsIgnoreCase("prod")) {
            System.out.println("se ejecuto el servicio consulta gedo: " + "  " + (new Date()).toString() + ": " + gson.toJson(pars));
        }
        boolean user = UserValidator.validarUsuario(pars.getUsuario(), clave, direcciones.get(Servicios.ldap));
        if (user) {
            result = consultadocumentows.ConsultaDocumentoWS.main2(pars.toParametros(direcciones));
        } else {
            result = "El usuario gde no existe o la contrasenia es incorrecta";
            throw new Exception(result);
        }*/
        return consultadocumentows.ConsultaDocumentoWS.main2(pars.toParametros(direcciones));
    }
}
