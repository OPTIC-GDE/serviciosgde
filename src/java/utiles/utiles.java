/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utiles;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;

/**
 *
 * @author joans
 */
public class utiles {
    public static InputStream dispatch(HttpURLConnection http) throws Exception {
        try {
            return http.getInputStream();
        } catch (Exception ex) {
            
            InputStreamReader isReader = new InputStreamReader(http.getErrorStream());
            BufferedReader reader = new BufferedReader(isReader);
            StringBuilder sb = new StringBuilder();
            String str;
            while ((str = reader.readLine()) != null) {
                sb.append(str);
            }
            throw new Exception(sb.toString());
        }
    }
}
