/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utiles;
import javax.jws.WebResult;
import javax.xml.bind.annotation.*;

@XmlRootElement(name = "TResultadoServicioFA")
@XmlAccessorType(XmlAccessType.FIELD)
public class TResultadoServicioFA {
    
    @XmlElement(name = "CodResultado")
    private int CodResultado;
    @XmlElement(name = "Resultado1")
    private byte[] Resultado1;
    @XmlElement(name = "Resultado2")
    private byte[] Resultado2;
    @XmlElement(name = "Resultado3")
    private byte[] Resultado3;
    @XmlElement(name = "Resultado4")
    private byte[] Resultado4;
    @XmlElement(name = "Resultado5")
    private byte[] Resultado5;
    @XmlElement(name = "FirmaResultado1")
    private byte[] FirmaResultado1;
    @XmlElement(name = "FirmaResultado2")
    private byte[] FirmaResultado2;
    @XmlElement(name = "FirmaResultado3")
    private byte[] FirmaResultado3;
    @XmlElement(name = "FirmaResultado4")
    private byte[] FirmaResultado4;
    @XmlElement(name = "FirmaResultado5")
    private byte[] FirmaResultado5;
    @XmlElement(name = "MensajeResultado")
    private String MensajeResultado;
    
        public int getCodResultado() {
            return CodResultado;
        }

    public void setCodResultado(int CodResutlado) {
        this.CodResultado = CodResutlado;
    }
    public byte[] getResultado1() {
        return Resultado1;
    }

    public void setResultado1(byte[] Resultado) {
        this.Resultado1 = Resultado;
    }

    public byte[] getResultado2() {
        return Resultado2;
    }

    public void setResultado2(byte[] Resultado2) {
        this.Resultado2 = Resultado2;
    }

    public byte[] getResultado3() {
        return Resultado3;
    }

    public void setResultado3(byte[] Resultado3) {
        this.Resultado3 = Resultado3;
    }

    public byte[] getResultado4() {
        return Resultado4;
    }

    public void setResultado4(byte[] Resultado4) {
        this.Resultado4 = Resultado4;
    }

    public byte[] getResultado5() {
        return Resultado5;
    }

    public void setResultado5(byte[] Resultado5) {
        this.Resultado5 = Resultado5;
    }

    public String getMensajeResultado() {
        return MensajeResultado;    
    }

    public void setMensajeResultado(String MensajeResultado) {
        this.MensajeResultado = MensajeResultado;
    }

    public byte[] getFirmaResultado1() {
        return FirmaResultado1;
    }

    public void setFirmaResultado1(byte[] FirmaResultado1) {
        this.FirmaResultado1 = FirmaResultado1;
    }

    public byte[] getFirmaResultado2() {
        return FirmaResultado2;
    }

    public void setFirmaResultado2(byte[] FirmaResultado2) {
        this.FirmaResultado2 = FirmaResultado2;
    }

    public byte[] getFirmaResultado3() {
        return FirmaResultado3;
    }

    public void setFirmaResultado3(byte[] FirmaResultado3) {
        this.FirmaResultado3 = FirmaResultado3;
    }

    public byte[] getFirmaResultado4() {
        return FirmaResultado4;
    }

    public void setFirmaResultado4(byte[] FirmaResultado4) {
        this.FirmaResultado4 = FirmaResultado4;
    }

    public byte[] getFirmaResultado5() {
        return FirmaResultado5;
    }

    public void setFirmaResultado5(byte[] FirmaResultado5) {
        this.FirmaResultado5 = FirmaResultado5;
    }
    
   
}

