/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utiles;

/**
 *
 * @author joans
 */
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

public class RSAUtil {

    private static String privateKey = "MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBALHmeeAxRodyBNVZDTdHXom+iENkHDRHYaAtQ7Tnli3SWtxjDm3YDat0lKQvz4gVHWPCcOQT/8P+XkEiCjOYDI84KwRhvqO65V7SRuaA/jwX0C15GG68/TblaWu1nsYR53XNWN+XbozNmRhh5XLDOR4xev0VdreHutksadMJfK9vAgMBAAECgYBvrLKNHeGcXOLT2tcFAHREG6sGQu3Bk52hI0kBYu9PTGFlP3lrUtkh7Ejy2GB6mGaenQyo4DUKH7+jO28T/DrFDqbFrzav083rFMwSRCG4OZ+VJbCnukl7tZ8/tVxDpx7EBQ+SMLlri70Lpe8kaix1aF7zkTkwe6AnLAbOIomqwQJBAPRP2PVz4OD8lHiB+ghkP3w4o85ygabvoGlu/Z7KOZTn0q2pS17q8tI7v221GOE4q4CJKlQXnkmjqi4E0WNaiykCQQC6aUSf9nAMrOK6AGsJ41ywjREDi9T5RaELIXNDPDyePMYe/dCagsdGjjYpwtFZfwT32xLY/OtdMysI9fS47VDXAkAGC+vct097ggTSrrTXfFOt6WD0fRUEYq3friWMxoiCTPKD5cq9wKbd2Si1uGOzP0nPVpW7mxIJ1Ikjtm7QPK9xAkAxHdOLIm/bpyIGSF3ruRCPJFw7RvjSxVntiN7J9HE0cGbze4UfcR34pTf4QPDQV/uDIyIQMnQBlp3cOpJEt6L5AkEA3Hdgby2n3RtKAjedaM7eJ9WxA3aADPVNxnUvRsA5/08BxQYdo7kLYWx71HqhoCdssPmVzjqsQnFtADIhaQkP8g==";
    private static String publicKey = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCx5nngMUaHcgTVWQ03R16JvohDZBw0R2GgLUO055Yt0lrcYw5t2A2rdJSkL8+IFR1jwnDkE//D/l5BIgozmAyPOCsEYb6juuVe0kbmgP48F9AteRhuvP025WlrtZ7GEed1zVjfl26MzZkYYeVywzkeMXr9FXa3h7rZLGnTCXyvbwIDAQAB";

    public static PublicKey getPublicKey(String base64PublicKey) {
        PublicKey publicKey = null;
        try {
            X509EncodedKeySpec keySpec = new X509EncodedKeySpec(Base64.getDecoder().decode(base64PublicKey.getBytes()));
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            publicKey = keyFactory.generatePublic(keySpec);
            return publicKey;
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        }
        return publicKey;
    }

    public static PrivateKey getPrivateKey(String base64PrivateKey) throws NoSuchAlgorithmException,InvalidKeySpecException {
        PrivateKey privateKey = null;
        PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(Base64.getDecoder().decode(base64PrivateKey.getBytes()));
        KeyFactory keyFactory = null;
        keyFactory = KeyFactory.getInstance("RSA");
        privateKey = keyFactory.generatePrivate(keySpec);
   
        return privateKey;
    }

    public static byte[] encrypt(String data, String publicKey) throws BadPaddingException, IllegalBlockSizeException, InvalidKeyException, NoSuchPaddingException, NoSuchAlgorithmException {
        Cipher cipher = Cipher.getInstance("RSA");
        cipher.init(Cipher.ENCRYPT_MODE, getPublicKey(publicKey));
        return cipher.doFinal(data.getBytes());
    }

    public static String decrypt(byte[] data, PrivateKey privateKey) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        Cipher cipher = Cipher.getInstance("RSA");
        cipher.init(Cipher.PRIVATE_KEY, privateKey);
        return new String(cipher.doFinal(data));
    }

    public static String decrypt(String data) throws IllegalBlockSizeException, InvalidKeyException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeySpecException {
        return decrypt(Base64.getDecoder().decode(data.getBytes()), getPrivateKey(privateKey));
    }


    public static String decryptTextUsingAES(String encryptedText, String aesKeyString) throws Exception {

        byte[] decodedKey = Base64.getDecoder().decode(aesKeyString);
        SecretKey originalKey = new SecretKeySpec(decodedKey, 0, decodedKey.length, "AES");

        // AES defaults to AES/ECB/PKCS5Padding in Java 7
        Cipher aesCipher = Cipher.getInstance("AES");
        aesCipher.init(Cipher.DECRYPT_MODE, originalKey);
        byte[] bytePlainText = aesCipher.doFinal(Base64.getDecoder().decode(encryptedText));
        return new String(bytePlainText);
    }

}
