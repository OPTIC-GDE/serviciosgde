/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utiles;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.thinknet.integrabilidad.autenticacion.IAutenticacionProxy;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;
import java.util.Hashtable;
import javax.naming.AuthenticationException;
import javax.naming.Context;
import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;

/**
 *
 * @author joans
 */
public class UserValidator {

    public static RespuestaUser validar(String username, String password, String ldap) throws RemoteException, MalformedURLException, IOException, NamingException, Exception {
        System.out.println(validarUsuario(username, password, ldap));
        return validarUsuarioV2(username, password, ldap);
    }

    public static RespuestaUser validar(String ticket) throws RemoteException, MalformedURLException, IOException, NamingException, Exception {
        return validarTicketPecas(ticket);
    }

    private static RespuestaUser validarTicketPecas(String ticket) throws RemoteException, MalformedURLException, IOException, Exception {

        String ipProd = "http://192.168.12.234";
        IAutenticacionProxy proxy = new IAutenticacionProxy();
        proxy.setEndpoint("http://autentica.neuquen.gov.ar:8080/scripts/autenticacion.exe/soap/IAutenticacion");
        RespuestaUser res = new RespuestaUser(proxy.verificarSesionActivaPecas_V2(ticket));

        if (res.isConectado()) {
            /**
             * obtengo el usuario de one login que se supone que es documento
             */
            res.setDocumento(proxy.obtenerDatosDeSesion(ticket).getUsuario());
            String direccion = ipProd + "/consultas/usuario/" + res.getDocumento();
            URL urlConsulta = new URL(direccion);
            HttpURLConnection con = (HttpURLConnection) urlConsulta.openConnection();
            con.setRequestProperty("content-type", "application/json");
            con.setRequestMethod("GET");

            InputStream responseStream = utiles.dispatch(con);

            InputStreamReader isReader = new InputStreamReader(responseStream);

            BufferedReader reader = new BufferedReader(isReader);
            StringBuilder sb = new StringBuilder();
            String str;
            while ((str = reader.readLine()) != null) {
                sb.append(str);
            }
            Gson gson = new Gson();

            JsonObject obj = gson.fromJson(sb.toString(), JsonObject.class);
            JsonPrimitive user = obj.getAsJsonPrimitive("usuario");
            res.setUsuario(user.getAsString());
        }

        return res;
    }

    public static boolean validarUsuario(String username, String password, String ldap) throws AuthenticationException, NamingException {
        String base = "ou=sade,dc=gob,dc=ar";
        String dn = "cn=" + username.toUpperCase() + "," + base;
        String ldapURL = ldap;
        boolean res = false;
        // Setup environment for authenticating
        Hashtable<String, String> environment
                = new Hashtable<String, String>();
        environment.put(Context.INITIAL_CONTEXT_FACTORY,
                "com.sun.jndi.ldap.LdapCtxFactory");
        environment.put(Context.PROVIDER_URL, ldapURL);
        environment.put(Context.SECURITY_AUTHENTICATION, "simple");
        environment.put(Context.SECURITY_PRINCIPAL, dn);
        environment.put(Context.SECURITY_CREDENTIALS, password);

        try {
            DirContext authContext
                    = new InitialDirContext(environment);
            res = true;
            // user is authenticated
        } catch (AuthenticationException ex) {
            res = false;
            // Authentication failed
        } catch (NamingException ex) {
            res = false;

        }
        return res;

    }

    private static RespuestaUser validarUsuarioV2(String username, String password, String ldap) throws AuthenticationException, NamingException {
        String base = "ou=sade,dc=gob,dc=ar";
        String dn = "cn=" + username.toUpperCase() + "," + base;
        String ldapURL = ldap;
        boolean res = false;
        RespuestaUser resp;

        // Setup environment for authenticating
        Hashtable<String, String> environment
                = new Hashtable<String, String>();
        environment.put(Context.INITIAL_CONTEXT_FACTORY,
                "com.sun.jndi.ldap.LdapCtxFactory");
        environment.put(Context.PROVIDER_URL, ldapURL);
        environment.put(Context.SECURITY_AUTHENTICATION, "simple");
        environment.put(Context.SECURITY_PRINCIPAL, dn);
        environment.put(Context.SECURITY_CREDENTIALS, password);

        try {
            DirContext authContext
                    = new InitialDirContext(environment);
            res = true;
            // user is authenticated
        } catch (AuthenticationException ex) {
            res = false;
            // Authentication failed
        } catch (NamingException ex) {
            res = false;

        }
        resp = new RespuestaUser(res);
        resp.setUsuario(username);
        return resp;

    }

}
