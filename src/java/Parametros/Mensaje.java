/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Parametros;

import com.google.gson.annotations.SerializedName;
import utiles.AnnotatedDeserializer;

/**
 *
 * @author joans
 */
public class Mensaje {

    @SerializedName("clave")
    private String clave;

    @SerializedName("ticket")
    private String ticket;
    
    @SerializedName("parametros")
    private String parametros;

    @SerializedName("archivo")
    private byte[] archivo;

    public byte[] getArchivo() {
        return archivo;
    }

    public void setArchivo(byte[] archivo) {
        this.archivo = archivo;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getParametros() {
        return parametros;
    }

    public void setParametros(String parametros) {
        this.parametros = parametros;
    }

    public String getTicket() {
        return ticket;
    }

    public void setTicket(String ticket) {
        this.ticket = ticket;
    }

}
