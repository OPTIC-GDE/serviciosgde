/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Parametros;

import static consultadocumentows.Parametros.*;
import com.google.gson.annotations.SerializedName;
import java.util.HashMap;
import java.util.LinkedList;
import Parametros.Servicios;
import utiles.AnnotatedDeserializer;

/**
 *
 * @author joans
 */
public class ConsultaDocumento extends Usuario {

    @SerializedName("numeroSade")
    private String numeroSade;

    @SerializedName("numeroEspecial")
    private String numeroEspecial;

    @SerializedName("numeroProceso")
    private String numeroProceso;
    
    @AnnotatedDeserializer.FieldRequired
    @SerializedName("sistema")
    private String sistema;

    public String getNumeroSade() {
        return numeroSade;
    }

    public void setNumeroSade(String numeroSade) {
        this.numeroSade = numeroSade;
    }

    public String getNumeroEspecial() {
        return numeroEspecial;
    }

    public void setNumeroEspecial(String numeroEspecial) {
        this.numeroEspecial = numeroEspecial;
    }

    public String getNumeroProceso() {
        return numeroProceso;
    }

    public void setNumeroProceso(String numeroProceso) {
        this.numeroProceso = numeroProceso;
    }

    public String getSistema() {
        return sistema;
    }

    public void setSistema(String sistema) {
        this.sistema = sistema;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getAmbiente() {
        return ambiente;
    }

    public void setAmbiente(String ambiente) {
        this.ambiente = ambiente;
    }

    public String[] toParametros(HashMap<String, String> map) {
        LinkedList<String> list = new LinkedList<>();

        if (usuario != null) {
            list.add(USUARIO_CONSULTA);
            list.add(usuario); 
        }

        list.add(URL);
        list.add(map.get(Servicios.consultaGedo));

        if (numeroEspecial != null) {
            list.add(NUMERO_ESPECIAL);
            list.add(numeroEspecial);
        }
        if (numeroProceso != null) {
            list.add(NUMERO_PROCESO);
            list.add(numeroProceso);
            
            list.add(SISTEMA_SUSCRIPTO);
            list.add(sistema);
        }
        if (numeroSade != null) {
            list.add(NUMERO_SADE);
            list.add(numeroSade);
        }

        return list.toArray(new String[list.size()]);
    }

}
