/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Parametros;
import static asociarexpedientews.Parametros.*;
import com.google.gson.annotations.SerializedName;
import static generarexpediente.Parametros.URL;
import static generarexpediente.Parametros.URL_BLOQUEADOR;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import utiles.AnnotatedDeserializer;

/**
 *
 * @author joans
 */
public class AsociarExpediente extends Usuario{
    
    @SerializedName("sistema")
    @AnnotatedDeserializer.FieldRequired
    private String sistema;
    
    @AnnotatedDeserializer.FieldRequired
    @SerializedName("expedientes")
    private String[] expedientes;

    @SerializedName("desasociar")
    private boolean desasociar;

    public String getSistema() {
        return sistema;
    }

    public void setSistema(String sistema) {
        this.sistema = sistema;
    }

    public String[] getExpedientes() {
        return expedientes;
    }

    public void setExpedientes(String[] expedientes) {
        this.expedientes = expedientes;
    }

    public boolean isDesasociar() {
        return desasociar;
    }

    public void setDesasociar(boolean desasociar) {
        this.desasociar = desasociar;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getAmbiente() {
        return ambiente;
    }

    public void setAmbiente(String ambiente) {
        this.ambiente = ambiente;
    }
    
    public String[] toParametros(HashMap<String, String> map) {
        LinkedList<String> list = new LinkedList<>();
        
        list.add(URL);
        list.add(map.get(Servicios.asociarExpedientes));

        list.add(URL_BLOQUEADOR);
        list.add(map.get(Servicios.bloqueoDeExpedientes));

        if (this.expedientes != null) {
            list.add(EXPEDIENTES);
            Arrays.stream(this.expedientes).forEach(e -> list.add(e));
        }

        if (this.sistema != null) {
            list.add(SISTEMA);
            list.add(sistema);
        }
        
        
       list.add(USUARIO);
       list.add(this.usuario);
       
       if(this.desasociar){
           list.add(DESASOCIAR);
       }
        for (int i = 0; i < list.size(); i++) {
           System.out.print(list.get(i) + " ");
        }
       return list.toArray(new String[list.size()]);
    }
}
