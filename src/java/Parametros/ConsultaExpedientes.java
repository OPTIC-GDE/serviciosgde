/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Parametros;

import com.google.gson.annotations.SerializedName;
import utiles.AnnotatedDeserializer;

/**
 *
 * @author joans
 */
public class ConsultaExpedientes extends Usuario {

    @SerializedName("trata")
    private String trata;

    @SerializedName("descripcion")
    private String descripcion;

    @SerializedName("anio")
    private String anio;

    @SerializedName("fechaDesde")
    private String fechaDesde;

    @SerializedName("fechaHasta")
    private String fechaHasta;

    @SerializedName("numero")
    private int numero;

    @SerializedName("creador")
    private String creador;

    @SerializedName("reparticion")
    private String reparticion;

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getAnio() {
        return anio;
    }

    public void setAnio(String anio) {
        this.anio = anio;
    }

    public String getTrata() {
        return trata;
    }

    public void setTrata(String trata) {
        this.trata = trata;
    }

    public String getFechaDesde() {
        return fechaDesde;
    }

    public void setFechaDesde(String fechaDesde) {
        this.fechaDesde = fechaDesde;
    }

    public String getFechaHasta() {
        return fechaHasta;
    }

    public void setFechaHasta(String fechaHasta) {
        this.fechaHasta = fechaHasta;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public String getCreador() {
        return creador;
    }

    public void setCreador(String creador) {
        this.creador = creador;
    }

    public String getReparticion() {
        return reparticion;
    }

    public void setReparticion(String reparticion) {
        this.reparticion = reparticion;
    }

    public String toParametros() {
        String res = "";
        String separador = "?";
        if (this.trata != null) {
            res += separador + "trata=" + this.trata;
            separador = "&";
        }
        if (this.fechaDesde != null) {
            res += separador + "fechaDesde=" + this.fechaDesde;
            separador = "&";
        }
        if (this.fechaHasta != null) {
            res += separador + "fechaHasta=" + this.fechaHasta;
            separador = "&";

        }
        if (this.numero != 0) {
            res += separador + "numero=" + this.numero;
            separador = "&";
        }
        if (this.creador != null) {
            res += separador + "creador=" + this.creador;
            separador = "&";

        }
        if (this.anio != null) {
            res += separador + "anio=" + this.anio;
            separador = "&";

        }
        if (this.descripcion != null) {
            res += separador + "descripcion=" + this.descripcion;
            separador = "&";

        }
        if (this.reparticion != null) {
            String repa = this.reparticion.replaceAll("#", "%23");
            res += separador + "reparticion=" + repa;
            separador = "&";

        }
        if (this.ambiente != null) {
            res += separador + "ambiente=" + ambiente;
            separador = "&";

        }

        return res;
    }
}
