/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Parametros;

import com.google.gson.annotations.SerializedName;
import static generarpase.Parametros.*;
import java.util.HashMap;
import java.util.LinkedList;
import Parametros.Servicios;
import utiles.AnnotatedDeserializer;

/**
 *
 * @author joans
 */
public class GenerarPase extends Usuario {
    
    @AnnotatedDeserializer.FieldRequired
    @SerializedName("expediente")
    private String expediente;

    @SerializedName("vaMesa")
    private boolean vaMesa;

    @SerializedName("vaReparticion")
    private boolean vaReparticion;

    @SerializedName("nuevoEstado")
    private String nuevoEstado;

    @AnnotatedDeserializer.FieldRequired
    @SerializedName("motivo")
    private String motivo;

    @SerializedName("reparticionDestino")
    private String reparticionDestino;

    @SerializedName("sectorDestino")
    private String sectorDestino;

    @AnnotatedDeserializer.FieldRequired
    @SerializedName("sistemaOrigen")
    private String sistemaOrigen;

    @SerializedName("usuarioDestino")
    private String usuarioDestino;

    public String getExpediente() {
        return expediente;
    }

    public void setExpediente(String expediente) {
        this.expediente = expediente;
    }

    public boolean isVaMesa() {
        return vaMesa;
    }

    public void setVaMesa(boolean vaMesa) {
        this.vaMesa = vaMesa;
    }

    public boolean isVaReparticion() {
        return vaReparticion;
    }

    public void setVaReparticion(boolean vaReparticion) {
        this.vaReparticion = vaReparticion;
    }

    public String getNuevoEstado() {
        return nuevoEstado;
    }

    public void setNuevoEstado(String nuevoEstado) {
        this.nuevoEstado = nuevoEstado;
    }

    public String getMotivo() {
        return motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    public String getReparticionDestino() {
        return reparticionDestino;
    }

    public void setReparticionDestino(String reparticionDestino) {
        this.reparticionDestino = reparticionDestino;
    }

    public String getSectorDestino() {
        return sectorDestino;
    }

    public void setSectorDestino(String sectorDestino) {
        this.sectorDestino = sectorDestino;
    }

    public String getSistemaOrigen() {
        return sistemaOrigen;
    }

    public void setSistemaOrigen(String sistemaOrigen) {
        this.sistemaOrigen = sistemaOrigen;
    }

    public String getUsuarioDestino() {
        return usuarioDestino;
    }

    public void setUsuarioDestino(String usuarioDestino) {
        this.usuarioDestino = usuarioDestino;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getAmbiente() {
        return ambiente;
    }

    @Override
    public void setAmbiente(String ambiente) {
        this.ambiente = ambiente;
    }

    public String[] toParametros(HashMap<String, String> map) {
        LinkedList<String> list = new LinkedList<>();

        list.add(URL);
        list.add(map.get(Servicios.generarPase));

        list.add(URL_BLOQUEADOR);
        list.add(map.get(Servicios.bloqueoDeExpedientes));

        if (expediente != null) {
            list.add(EXPEDIENTE);
            list.add(this.expediente);
        }
        if (motivo != null) {
            list.add(MOTIVO);
            list.add(this.motivo);
        }

        if (this.nuevoEstado != null) {
            list.add(NUEVO_ESTADO);
            list.add(this.nuevoEstado);
        }

        if (vaMesa) {
            list.add(VA_MESA);
        }

        if (vaReparticion) {
            list.add(VA_REPARTICION);
        }

        if (motivo != null) {
            list.add(MOTIVO);
            list.add(this.motivo);
        }

        if (this.usuario != null) {
            list.add(USUARIO_ORIGEN);
            list.add(this.usuario);
        }

        if (this.reparticionDestino != null) {
            list.add(REPARTICION_DESTINO);
            list.add(this.reparticionDestino);
        }

        if (this.sectorDestino != null) {
            list.add(SECTOR_DESTINO);
            list.add(this.sectorDestino);
        }
        if (this.usuarioDestino != null) {
            list.add(USUARIO_DESTINO);
            list.add(this.usuarioDestino);
        }
        if (sistemaOrigen != null) {
            list.add(SISTEMA_ORIGEN);
            list.add(sistemaOrigen);
        }

        return list.toArray(new String[list.size()]);

    }

}
