/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Parametros;

import static consultadocumentows.Parametros.*;
import com.google.gson.annotations.SerializedName;
import java.util.HashMap;
import java.util.LinkedList;
import Parametros.Servicios;
import utiles.AnnotatedDeserializer;

/**
 *
 * @author joans
 */
public class DetalleExpediente extends Usuario {

    @SerializedName("nroSade")
    private String nroSade;

    @SerializedName("anio")
    private String anio;

    @SerializedName("numero")
    private int numero;

    public String getNroSade() {
        return nroSade;
    }

    public void setNroSade(String numeroSade) {
        this.nroSade = numeroSade;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getAmbiente() {
        return ambiente;
    }

    public void setAmbiente(String ambiente) {
        this.ambiente = ambiente;
    }

    public String getAnio() {
        return anio;
    }

    public void setAnio(String anio) {
        this.anio = anio;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public String toParametros() throws Exception{
        String res = "";
        String separador = "?";
        if(this.numero != 0 && this.anio != null){
            res = "/"+anio+"/"+numero;
        }else if(this.nroSade != null){
            res = "/"+this.nroSade.replaceAll("#", "%23").replaceAll(" ", "");
        }else{
            String mensaje = "Porfavor envie el nroSade o numero de sistema y anio";
            throw new Exception(mensaje);
        }
        
        if (this.ambiente != null) {
            res += separador + "ambiente=" + ambiente;
        }
        return res;
    }

}
