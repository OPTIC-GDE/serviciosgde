/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Parametros;

import static documentosoficialesws.Parametros.*;
import com.google.gson.annotations.SerializedName;
import java.util.HashMap;
import java.util.LinkedList;
import Parametros.Servicios;
import utiles.AnnotatedDeserializer;

/**
 *
 * @author joans
 */
public class VincularDocumento extends Usuario {
    @AnnotatedDeserializer.FieldRequired
    @SerializedName("sistema")
    protected String sistema;
    
    @AnnotatedDeserializer.FieldRequired
    @SerializedName("expediente")
    protected String expediente;
    
    @AnnotatedDeserializer.FieldRequired
    @SerializedName("documentos")
    protected String[] documentos;

    public String getSistema() {
        return sistema;
    }

    public void setSistema(String sistema) {
        this.sistema = sistema;
    }

    public String getExpediente() {
        return expediente;
    }

    public void setExpediente(String expediente) {
        this.expediente = expediente;
    }

    public String[] getDocumentos() {
        return documentos;
    }

    public void setDocumentos(String[] documentos) {
        this.documentos = documentos;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }


    public String getAmbiente() {
        return ambiente;
    }

    public void setAmbiente(String ambiente) {
        this.ambiente = ambiente;
    }

    public String[] toParametros(HashMap<String, String> map) {
        LinkedList<String> list = new LinkedList<>();
        list.add(URL_BLOQUEADOR);
        list.add(map.get(Servicios.bloqueoDeExpedientes));

        list.add(URL);
        list.add(map.get(Servicios.vincularDocumento));

        if (sistema != null) {
            list.add(SISTEMA);
            list.add(this.sistema);
        }
        if (expediente != null) {
            list.add(EXPEDIENTE);
            list.add(this.expediente);
        }
        if (usuario != null) {
            list.add(USUARIO);
            list.add(this.usuario);
        }
        if (documentos != null && documentos.length > 0) {

            list.add(DOCUMENTOS);
            for (int i = 0; i < this.documentos.length; i++) {
                list.add(this.documentos[i]);
            }
        }
        
        return list.toArray(new String[list.size()]);

    }

}
