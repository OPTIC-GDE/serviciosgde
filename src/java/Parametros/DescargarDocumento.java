/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Parametros;

import com.google.gson.annotations.SerializedName;
import static descargardocumentows.Parametros.*;
import java.util.HashMap;
import java.util.LinkedList;

/**
 *
 * @author joans
 */
public class DescargarDocumento extends Usuario {
    @SerializedName("numeroSade")
    private String numeroSade;

    public DescargarDocumento() {
    }

    public String getNumeroSade() {
        return numeroSade;
    }

    public void setNumeroSade(String numeroSade) {
        this.numeroSade = numeroSade;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getAmbiente() {
        return ambiente;
    }

    public void setAmbiente(String ambiente) {
        this.ambiente = ambiente;
    }
     public String[] toParametros(HashMap<String, String> map) {
        LinkedList<String> list = new LinkedList<>();

        list.add(URL);
        list.add(map.get(Servicios.descargarGedo));

        list.add(USUARIO);
        list.add(this.getUsuario());
                
        list.add(NUMERO_SADE);
        list.add(this.getNumeroSade());
        
        
        return list.toArray(new String[list.size()]);

     }
     
    
}
