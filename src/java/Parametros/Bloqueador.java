/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Parametros;

import Parametros.Servicios;
import static main.Parametros.*;
import com.google.gson.annotations.SerializedName;
import java.util.HashMap;
import java.util.LinkedList;
import utiles.AnnotatedDeserializer;

/**
 *
 * @author joans
 */
public class Bloqueador extends Usuario {

    @SerializedName("sistema")
    @AnnotatedDeserializer.FieldRequired
    private String sistema;
    
    @AnnotatedDeserializer.FieldRequired
    @SerializedName("expediente")
    private String expediente;

    @SerializedName("bloquear")
    private boolean bloquear;

    public String getSistema() {
        return sistema;
    }

    public void setSistema(String sistema) {
        this.sistema = sistema;
    }

    public String getExpediente() {
        return expediente;
    }

    public void setExpediente(String expediente) {
        this.expediente = expediente;
    }

    public boolean isBloquear() {
        return bloquear;
    }

    public void setBloquear(boolean bloquear) {
        this.bloquear = bloquear;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }


    public String getAmbiente() {
        return ambiente;
    }

    public void setAmbiente(String ambiente) {
        this.ambiente = ambiente;
    }

    public String[] toParametros(HashMap<String, String> map) {
        LinkedList<String> list = new LinkedList<>();
        
        list.add(URL);
        list.add(map.get(Servicios.bloqueoDeExpedientes));

        if (expediente != null) {
            list.add(EXPEDIENTE);
            list.add(this.expediente);
        }

        if (sistema != null) {
            list.add(SISTEMA);
            list.add(sistema);
        }

        if (bloquear) {
            list.add(BLOQUEAR);
        }

        return list.toArray(new String[list.size()]);
    }
}
