/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Parametros;

/**
 *
 * @author joans
 */
public interface Servicios {

    public static String vincularDocumento = "vincularDocumento";
    public static String generarTarea = "generarTarea";
    public static String generarDocumento = "generarDocumento";    
    public static String detalleExpediente = "detalleExpediente";
    public static String generarExpediente = "generarExpediente";
    public static String generarPase = "generarPase";
    public static String bloqueoDeExpedientes = "bloqueoDeExpedientes";
    public static String consultaGedo = "consultaGedo";
    public static String descargarGedo = "descargarDocumento";
    public static String asociarExpedientes = "asociarExpedientes";    
    public static String consultarExpedientes = "consultarExpedientes";
    public static String ldap = "ldap";

}
