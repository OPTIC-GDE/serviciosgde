/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Parametros;

import com.google.gson.annotations.SerializedName;
import static generarexpediente.Parametros.*;
import java.util.HashMap;
import java.util.LinkedList;
import Parametros.Servicios;
import utiles.AnnotatedDeserializer;

/**
 *
 * @author joans
 */
public class GenerarExpediente extends Usuario {

    @SerializedName("email")
    private String email;

    @SerializedName("telefono")
    private String telefono;
    
    @AnnotatedDeserializer.FieldRequired
    @SerializedName("sistema")
    private String sistema;
    
    @AnnotatedDeserializer.FieldRequired
    @SerializedName("trata")
    private String trata;

    @SerializedName("descripcion")
    private String descripcion;
    
    @AnnotatedDeserializer.FieldRequired
    @SerializedName("motivo")
    private String motivo;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getSistema() {
        return sistema;
    }

    public void setSistema(String sistema) {
        this.sistema = sistema;
    }

    public String getTrata() {
        return trata;
    }

    public void setTrata(String trata) {
        this.trata = trata;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getMotivo() {
        return motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }


    public String getAmbiente() {
        return ambiente;
    }

    public void setAmbiente(String ambiente) {
        this.ambiente = ambiente;
    }

    public String[] toParametros(HashMap<String, String> map) {
        LinkedList<String> list = new LinkedList<>();
        if (usuario != null) {
            list.add(CARATULADOR);
            list.add(this.usuario);
        }

        list.add(URL);
        list.add(map.get(Servicios.generarExpediente));

        list.add(URL_BLOQUEADOR);
        list.add(map.get(Servicios.bloqueoDeExpedientes));
        
        list.add(ES_EXTERNO);
        list.add("no");

        if (this.email != null) {
            list.add(EMAIL);
            list.add(this.email);
        }
        if (this.telefono != null) {
            list.add(TELEFONO);
            list.add(this.telefono);
        }
        if (this.descripcion != null) {
            list.add(DESCRIPCION);
            list.add(this.descripcion);
        }
        if (motivo != null) {
            list.add(MOTIVO);
            list.add(this.motivo);
        }
        list.add(CODIGO_TRATA);
        list.add(trata);

        if (sistema != null) {
            list.add(SISTEMA);
            list.add(this.sistema);
        }

        return list.toArray(new String[list.size()]);
    }

}
