/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Parametros;

import com.google.gson.annotations.SerializedName;
import static generardocumentows.Parametros.*;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import Parametros.Servicios;
import utiles.AnnotatedDeserializer;

/**
 *
 * @author joans
 */
public class GenerarDocumento extends Usuario {
    
    @AnnotatedDeserializer.FieldRequired
    @SerializedName("acronimo")
    private String acronimo;

    @SerializedName("destinatarios")
    private String[] destinatarios;

    @SerializedName("destinatariosCopia")
    private String[] destinatariosCopia;

    @SerializedName("destinatariosCopiaOculta")
    private String[] destinatariosCopiaOculta;

    @SerializedName("mensaje")
    private String mensaje;
    
    @AnnotatedDeserializer.FieldRequired
    @SerializedName("referencia")
    private String referencia;
    
    @AnnotatedDeserializer.FieldRequired
    @SerializedName("sistemaOrigen")
    private String sistemaOrigen;
    
    @AnnotatedDeserializer.FieldRequired
    @SerializedName("tipoArchivo")
    private String tipoArchivo;

    public String getAcronimo() {
        return acronimo;
    }

    public void setAcronimo(String acronimo) {
        this.acronimo = acronimo;
    }

    public String[] getDestinatarios() {
        return destinatarios;
    }

    public void setDestinatarios(String[] destinatarios) {
        this.destinatarios = destinatarios;
    }

    public String[] getDestinatarioCopia() {
        return destinatariosCopia;
    }

    public void setDestinatarioCopia(String[] destinatarioCopia) {
        this.destinatariosCopia = destinatarioCopia;
    }

    public String[] getDestinatarioCopiaOculta() {
        return destinatariosCopiaOculta;
    }

    public void setDestinatarioCopiaOculta(String[] destinatarioCopiaOculta) {
        this.destinatariosCopiaOculta = destinatarioCopiaOculta;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    public String getSistemaOrigen() {
        return sistemaOrigen;
    }

    public void setSistemaOrigen(String sistemaOrigen) {
        this.sistemaOrigen = sistemaOrigen;
    }

    public String getTipoArchivo() {
        return tipoArchivo;
    }

    public void setTipoArchivo(String tipoArchivo) {
        this.tipoArchivo = tipoArchivo;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }
    
    public String getAmbiente() {
        return ambiente;
    }

    public void setAmbiente(String ambiente) {
        this.ambiente = ambiente;
    }

    public String[] toParametros(HashMap<String, String> map) {
        LinkedList<String> list = new LinkedList<>();
        list.add(ACRONIMO);
        list.add(acronimo);

        list.add(URL);
        list.add(map.get(Servicios.generarDocumento));

        if (destinatarios != null) {
            list.add(DESTINATARIOS);
            Arrays.stream(destinatarios).forEach(e -> list.add(e));
        }

        if (destinatariosCopia != null) {
            list.add(DESTINATARIO_COPIA);
            Arrays.stream(destinatariosCopia).forEach(e -> list.add(e));
        }

        if (destinatariosCopiaOculta != null) {
            list.add(DESTINATARIO_COPIA_OCULTA);
            Arrays.stream(destinatariosCopiaOculta).forEach(e -> list.add(e));
        }

        if (mensaje != null) {
            list.add(MENSAJE);
            list.add(mensaje);
        }

        list.add(REFERENCIA);
        list.add(referencia);
        if (sistemaOrigen != null) {
            list.add(SISTEMA_ORIGEN);
            list.add(sistemaOrigen);

        }
        list.add(ARCHIVO);
        list.add("archivo");
        if (tipoArchivo != null) {
            list.add(TIPO_ARCHIVO);
            list.add(tipoArchivo);
        }
        if (usuario != null) {
            list.add(USUARIO);
            list.add(usuario);
        }

        return list.toArray(new String[list.size()]);
    }
}
